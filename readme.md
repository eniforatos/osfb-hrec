# In this repository 

This repo contains the following documents relating to OSFB:
- The Erasmsus MC data managment plan (DMP)
- The Erasmus MC DMP write-up guidelines  
- The TU Delft DMP for WP4
- The HREC form for all planned WP4 activities 
- Informed consent forms + opening statements for all planned WP4 activities 
- The associated DMP and HREC documents related to study one in WP3 of OSFB

These documents include important information such as: 
- Project grant number with NWO 
- PaNaMa project ID for human research 
- Prinicipal Investigator details for OSFB Consurtium



# Future work 
Languages to translate informed consent forms to, or provide informed consent forms in: 
- English (Default) 
- English (5th grade reading level) 
- Dutch (Default) 
- Dutch (5th grade reading level)
- Turkish 
- Arabic 
- Spanish 
- French 
- Portoguese
